package com.stoof.IM.GUI.server;

/**
 *Class Receiver is a threaded class used for receiving messages from the server
 *Internetprogrammering 1 - Course
 *@author Martin Carlsson
 */
import java.net.*;
import java.io.*;

import com.stoof.IM.GUI.MainChat;


public class Receiver implements Runnable
{

    private BufferedReader in;
    private Boolean run_thread;
    private MainChat chat;

    public Receiver(MainChat chat, Socket theSocket )
    {

        this.chat = chat;
    	run_thread = true;
        try
        {
            in = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
        }
        catch (IOException ioe)
        {
        }
    }

    /*Reads messages from the server and makes callbacks to the GUI class*/
    public void run( )
    {
        String inStream;
        while (run_thread)
        {
            try
            {
                if ((inStream = in.readLine()) != null)
                {
                    System.out.println(inStream);
                    this.chat.setIncomingMessage(inStream);
                }
            }
            catch (IOException ioe)
            {
            	System.out.println("ERROR");
            }
        }
        System.out.println("Thread Stopped");
    }

    public void stop( )
    {
        run_thread = false;
    }

    /*Closes the stream*/
    public void closeInStream( )
    {
        try
        {
            if (in != null)
                in.close();
        }
        catch (IOException iE)
        {
        }
    }
} // End class Receiver 