package com.stoof.IM.GUI.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;



public class Sender 
{
	private Socket echoSocket = null;
	private OutputStreamWriter out = null;
    private InputStream in = null;
    private BufferedReader stdIn = null;
    private URL url;
	public Sender(Socket temp) throws IOException
	{
		try
		{
			url = new URL("http://open.dkljjh.com/testIM.php");
            URLConnection connection = url.openConnection(); 
            connection.setDoOutput(true); 
            out = new OutputStreamWriter(connection.getOutputStream());
            InputStream in=connection.getInputStream();
            in.read();
			
		}catch(UnknownHostException uhe)
		{
			System.err.println("Unknown host trying to be reach");
			System.exit(1);
		}catch(IOException ioe)
		{
			System.err.println("Null connection to socket");
		}
		
        this.stdIn = new BufferedReader( new InputStreamReader(System.in));
	}
	
	public void sendAway(String message) throws IOException
	{
		this.out.write(message);
		out.flush();
	}
	
	public void closeConnection() throws IOException
	{
		this.out.close();
		this.in.close();
		this.stdIn.close();
		this.echoSocket.close();
	}


} // End class Sender