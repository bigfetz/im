package com.stoof.IM.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 * Creates the side panel of the instant messenger. Shows list of users
 * 
 * @author Stoof
 */
public class SidePanel extends JPanel implements ActionListener, MouseListener
{
    private static final long serialVersionUID = 1L;
    private final JList user_list; 
    private final DefaultListModel list_model;
    private final JPopupMenu user_menu;
    
    public SidePanel()
    {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(150, 0));
        
        list_model = new DefaultListModel();
        list_model.addElement(getUserName());
        
        user_list = new JList();
        user_list.setModel(list_model);
        user_list.setBackground(Color.lightGray);
        user_list.addMouseListener(this);
        
        user_menu = new JPopupMenu();
        user_menu.add("L2CodeLikeAPro");
        user_menu.add("Stoof Clan!");
        
        add(user_list, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
    }

    @Override
    public void mouseClicked( MouseEvent e )
    {
    }

    @Override
    public void mouseEntered( MouseEvent e )
    {
    }

    @Override
    public void mouseExited( MouseEvent e )
    {
    }

    @Override
    public void mousePressed( MouseEvent e )
    {
    }

    @Override
    public void mouseReleased( MouseEvent e )
    {
        if (user_menu.isPopupTrigger(e) && user_list.getSelectedIndex() != -1)
        {
            user_menu.show(user_list, e.getX(), e.getY());
        }
        
    }
    
    private String getUserName( )
    {
        return System.getProperty("user.name");
    }
}