package com.stoof.IM.GUI;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

import com.stoof.IM.utils.FontChooser;
import com.stoof.IM.utils.Utils;

public class MenuBar extends JMenuBar implements ActionListener
{
    private static final long serialVersionUID = 1L;
    private final JMenu file_menu, tools_menu, help_menu;
    private final JMenuItem quit, open, save;
    static JMenuItem Font;
    private final JMenuItem about;
    private final JMenuItem clear;
    static MainChat chat;
    Utils allUtils;

    public MenuBar(MainChat chat)
    {
        //I typically dont use "this." since it is implied and not necessary
        //file
    	allUtils = new Utils(chat);
        this.file_menu = new JMenu("File");
        //open
        this.open = new JMenuItem("Open");
        this.open.setMnemonic('O');
        this.open.addActionListener(this);
        this.file_menu.add(this.open);
        //end open
        //save
        this.save = new JMenuItem("Save");
        this.save.setMnemonic('S');
        this.save.addActionListener(this);
        this.file_menu.add(this.save);
        //end save
        //quit
        this.quit = new JMenuItem("Quit");
        this.quit.setMnemonic('Q');
        this.quit.addActionListener(this);
        this.file_menu.add(this.quit);
        //end quit

        //end file  
        //tools
        this.tools_menu = new JMenu("Tools");
        //Settings
        MenuBar.Font = new JMenuItem("Font");
        MenuBar.Font.setMnemonic('O');
        MenuBar.Font.addActionListener(this);
        this.tools_menu.add(MenuBar.Font);
        //end Settings
        //clear
        this.clear = new JMenuItem("Clear");
        this.clear.setMnemonic('C');
        this.clear.addActionListener(this);
        this.tools_menu.add(this.clear);
        //end Settings
        //end tools 
        //help  
        this.help_menu = new JMenu("Help");
        //about
        this.about = new JMenuItem("About");
        this.about.setMnemonic('A');
        this.about.addActionListener(this);
        this.help_menu.add(this.about);
        //end about
        //end help  

        //final add
        add(file_menu);
        add(tools_menu);
        add(help_menu);
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == quit)
        {
            if (Utils.quitMenuPopup() == 0)
                System.exit(0);
        }
        else if (e.getSource() == save)
        {
            try
            {
                Utils.saveFile(MenuBar.chat.getAllData());
            }
            catch (IOException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        else if (e.getSource() == Font)
        {
            //open options window
			try {
				//MainChat.currentFont = allUtils.fontMenu();
				@SuppressWarnings("unused")
				FontChooser fc = new FontChooser(chat);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

        }
        else if (e.getSource() == open)
        {
            try
            {
            	Utils.openFile();
            }
            catch (FileNotFoundException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        else if (e.getSource() == about)
        {
            //use util to popup about
            try
            {
                File f = new File("/Users/matthewfetzer/Dropbox/stoof/IM/Stoof_IM/misc/about.txt");
                Scanner scan = new Scanner(f);
                
                JTextArea at = new JTextArea();
                while (scan.hasNext())
                    at.setText(at.getText() + "\n" + scan.nextLine());
                at.setEditable(false);
                about.setSize(new Dimension(125, 125));
                about.add(at);
                about.setLocation(250, 250);
                about.setVisible(true);
            }
            catch (FileNotFoundException e1)
            {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        else if (e.getSource() == clear)
        {
            MenuBar.chat.clear();
        }

    }

    public void createTextField( MainChat chat )
    {
        MenuBar.chat = chat;
    }

}
