package com.stoof.IM.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.ConnectException;

import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.stoof.IM.GUI.server.Receiver;
import com.stoof.IM.GUI.server.Sender;

/**
 * Creates the main chat area including the input field and send button.
 * 
 * @author Stoof
 */
public class MainChat extends JPanel implements ActionListener, KeyListener
{
    private static final long serialVersionUID = 1L;
    private final JTextArea chat_area;
    private final JTextField text_input;
    private final JButton send_button;
    private final JPanel input_panel;
    private final JScrollPane scroll_pane;
    static Font currentFont;
    private Socket theSocket;
    private Receiver receiver;
    private Sender sender;
    private Thread in_thread;

    public MainChat()
    {
        setLayout(new BorderLayout());
        chat_area = new JTextArea();
        chat_area.setFont(currentFont);
        chat_area.setEditable(false);

        text_input = new JTextField();
        text_input.setFont(currentFont);
        text_input.setPreferredSize(new Dimension(0, 50));
        text_input.addActionListener(this);

        send_button = new JButton("SEND");
        send_button.setToolTipText("Send message.");
        send_button.addActionListener(this);

        scroll_pane = new JScrollPane(chat_area);

        input_panel = new JPanel();
        input_panel.setLayout(new BorderLayout());

        input_panel.add(text_input, BorderLayout.CENTER);
        input_panel.add(send_button, BorderLayout.EAST);

        add(scroll_pane, BorderLayout.CENTER);
        add(input_panel, BorderLayout.SOUTH);
    }

    public void ServerConnection(MainChat chat)
    {
        try
        {
            theSocket = new Socket("localhost",2000);
            
            sender = new Sender(theSocket);
            receiver = new Receiver(chat, theSocket); 
            
            if (in_thread == null)
            {
                in_thread = new Thread(receiver);
                in_thread.start(); // Starts the Receiver thread
            }
        }
        catch (UnknownHostException unknownHost)
        {
            System.err.println("You are trying to connect to an unknown host!");
        }
        catch (ConnectException connectE)
        {
            System.err.println("You are trying to connect to an unknown host!");
        }
        catch (IOException ioException)
        {
            ioException.printStackTrace();
        }

    }

    public void sendIt( String s ) throws IOException
    {
        sender.sendAway(s);
    }

    public void closeServerConnection( )
    {
        try
        {
            receiver.stop();
            sender.closeConnection();
            receiver.closeInStream();

            if (theSocket != null)
                theSocket.close();
            
            sender = null;
            receiver = null; 
        }
        catch (IOException iE)
        {
        	System.out.println("you got IOException");
        }
        catch (NullPointerException nE)
        {
        	System.out.println("you got NullPointerException");
        }
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == send_button || e.getSource() == text_input)
        {
            try
            {
            	setChatText(getAllData());
            	sender.sendAway(getAllData());
            }
            catch (Exception ex)
            {
            }
        }
    }


    @Override
    public void keyTyped( final KeyEvent e )
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run( )
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER && e.getModifiers() == 0)
                {
                	
                }
            }
        });
    }

    public String getAllData( )
    {
    	if (chat_area.getText() != null)
            return text_input.getText();
        else
            return " ";
    }

    public void clear( )
    {
        chat_area.setText("");
    }

    public void setFontSetting(Font temp)
    {
        if(temp ==  null)
        	System.out.println("the font is null");
    	chat_area.setFont(temp);
    }

    public void setChatText( String str )
    {
        if(chat_area.getText().length() > 0)
        	chat_area.setText(chat_area.getText() + "\n" + str);
        else
        	chat_area.setText(str);
    }

    public void setIncomingMessage( String s )
    {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
        Date date = new Date();
        chat_area.setText(getAllData() + ( String ) dateFormat.format(date) + " -" + s + '\n');
        text_input.setText("");
    }

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}