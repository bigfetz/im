package com.stoof.IM.GUI;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.JFrame;

/**
 * 
 * The main GUI application.
 * 
 * @author Stoof
 */
public class GUI extends JFrame
{
    private static final long serialVersionUID = 1L;

    /**
     * Create the application.
     * @throws IOException 
     * @throws UnknownHostException 
     */
    public GUI() throws UnknownHostException, IOException
    {        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 600);
        setMinimumSize(new Dimension(400,300));
        
        Container main_container = getContentPane();
        main_container.setLayout(new BorderLayout());

        final MainChat chat_box = new MainChat();
        main_container.add(chat_box, BorderLayout.CENTER);

        SidePanel user_sp = new SidePanel();
        main_container.add(user_sp, BorderLayout.WEST);
        
        MenuBar menu_bar = new MenuBar(chat_box);
        main_container.add(menu_bar, BorderLayout.NORTH);
        menu_bar.createTextField(chat_box);
        
        chat_box.ServerConnection(chat_box);
        
        addWindowListener(new WindowAdapter()
        {
            public void windowClosing( WindowEvent event )
            {
                
            	chat_box.closeServerConnection();
                System.out.println("Closing");
                System.exit(0);
            }

        });
          
    }

}
