package com.stoof.IM.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.stoof.IM.GUI.MainChat;



public class FontChooser implements ActionListener {

	final String[] fontSizes = {"8", "9", "10", "11", "12", "14", "16", "18", "20","22", "24", "26", "28", "36", "48", "72"};
	
	private String fontStyle = null;
	private String testString = "   abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private int fontSize;
	private Boolean isBold = false;
	private Boolean isUnderlined = false;
	private Boolean isItalic = false;
	private String[] fontNames = null;
	private JPanel sample = null;
	private JPanel fontStyleView = null;
	private JPanel fontSizeView = null;
	private JPanel checkBoxView = null;
	private JPanel sector1 = null;
	private JPanel sector2 = null;
	private JPanel sector3 = null;
	private JList fontSizeList = null;
	private JList fontStyleList = null;
    private JTextField fontSampleText = null;
    private JTextField fontSizeInput = null;
    private JCheckBox bold = null;
    private JCheckBox italic = null;
    private JButton applyButton = null;
    private Font currentFont = null;
    static Font appliedFont = null;
    MainChat tempMainChat;
	
	
	
	public FontChooser(MainChat temp) throws FileNotFoundException
	{
		tempMainChat = temp;
		setDefaultSettings(new File("textProps.txt"));
		JFrame main = new JFrame("Font");
		JPanel container = new JPanel();
		container.setSize(100,100);
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		this.fontNames = getFontFamilies();
		setSector1();
		setSector2();
		setSector3();
		main.setSize(new Dimension(440,270));

		JPanel top = new JPanel();
		JPanel bottom = new JPanel();
		bottom.setSize(500, 100);
		top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
		top.add(this.sector1);
		top.add(this.sector2);
		container.add(top);
		bottom.add(this.sector3);
		container.add(bottom);
		main.add(container);
		main.setVisible(true);
		
		System.out.print("Font panel created and should be showing");
		
	}
	public void setSector1()
	{
		setFontStyleView();
		this.sector1 = new JPanel();
		this.sector1.setLayout(new BoxLayout(this.sector1, BoxLayout.X_AXIS));
		this.sector1.add(this.fontStyleView);
	}
	
	public void setSector2()
	{
		setfontSizeView();
		setCheckBoxView();
		applyButton();
		this.sector2 = new JPanel();
		JPanel right = new JPanel();
		JPanel space = new JPanel();
		this.sector2.setLayout(new BoxLayout(this.sector2, BoxLayout.X_AXIS));
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		
		this.sector2.add(this.fontSizeView);
		right.add(this.checkBoxView);
		right.add(space);
		right.add(this.applyButton, BorderLayout.SOUTH);
		this.sector2.add(right);
	}
	
	public void setSector3()
	{
		setSamplePanel();
		this.sector3 = new JPanel();
		this.sector3.setMinimumSize(new Dimension(440,100));
		this.sector3.add(this.sample);
	}
	
	public void setSamplePanel()
	{
		
		this.sample = new JPanel();
		this.sample.setLayout(new BorderLayout());
		this.fontSampleText = new JTextField();
		this.fontSampleText.setPreferredSize(new Dimension(440,90));
		this.fontSampleText.getPreferredSize();
		this.fontSampleText.setText(this.testString);
		this.fontSampleText.setEditable(false);
		this.sample.add(this.fontSampleText);
	}
	
	public void setFontStyleView()
	{
		JTextArea name = new JTextArea();
		this.fontStyleView = new JPanel();	
		JPanel left = new JPanel();
		this.fontStyleList = new JList(this.fontNames);
		JScrollPane scrollPane = new JScrollPane(this.fontStyleList);
		
		this.fontStyleList.addMouseListener( new MouseAdapter(){
		    public void mouseClicked(MouseEvent e) {
		        if (e.getClickCount() == 1) {		           
		        	fontStyle = (String) fontStyleList.getSelectedValue();
		        	System.out.println(fontStyle);
		        	setFont();
		        	fontSampleText.setFont(currentFont);
				    fontSampleText.setText(testString);
		         }}}
			
		    );
		
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
		this.fontStyleList.ensureIndexIsVisible(this.fontStyleList.getSelectedIndex());
		this.fontStyleView.setLayout(new BoxLayout(this.fontStyleView, BoxLayout.X_AXIS));
		name.setText("Font Selection");
		name.setBackground(Color.GRAY);
		name.setMaximumSize(new Dimension(300,35));
		name.setEditable(false);
		left.add(name);
	    left.add(scrollPane);
	    this.fontStyleView.add(left);
	    

	}
	public void setfontSizeView()
	{
		this.fontSizeView = new JPanel();
		this.fontSizeInput = new JTextField();
		this.fontSizeList = new JList(fontSizes);
		JTextArea name = new JTextArea();
		
		JPanel leftSect = new JPanel();
		JScrollPane scrollPane = new JScrollPane(this.fontSizeList);
		this.fontSizeList.ensureIndexIsVisible(this.fontSizeList.getSelectedIndex());
		this.fontSizeView.setLayout(new BoxLayout(this.fontSizeView, BoxLayout.X_AXIS));
		leftSect.setLayout(new BoxLayout(leftSect, BoxLayout.Y_AXIS));
		
		this.fontSizeList.addMouseListener( new MouseAdapter(){
		    public void mouseClicked(MouseEvent e) {
		        if (e.getClickCount() == 1) {		           
		        	fontSize = Integer.parseInt((String) fontSizeList.getSelectedValue());  
		        	System.out.println(fontSize);
		        	setFont();
		        	fontSampleText.setFont(currentFont);
				    fontSampleText.setText(testString);
		         }}}
		    );
		
		this.fontSizeInput.addActionListener(this);
		this.fontSizeInput.setMaximumSize(new Dimension(57,35));
		scrollPane.setMaximumSize(new Dimension(50,400));
		
		name.setText("Font Size");
		name.setBackground(Color.GRAY);
		name.setMaximumSize(new Dimension(300,35));
		name.setEditable(false);
		
		leftSect.add(name);
		leftSect.add(this.fontSizeInput);
		leftSect.add(scrollPane);

		this.fontSizeView.add(leftSect);
	}
	
	public void setCheckBoxView()
	{
		this.checkBoxView = new JPanel();
		this.checkBoxView.setLayout(new BoxLayout(this.checkBoxView, BoxLayout.Y_AXIS));
		this.bold = new JCheckBox("Bold");
		this.italic = new JCheckBox("Italic");
		this.checkBoxView.add(this.bold);
		this.checkBoxView.add(this.italic);
		this.bold.setToolTipText("Bold.");
		this.bold.addActionListener(this);
		this.italic.setToolTipText("Italic.");
		this.italic.addActionListener(this);
	}
	
	public void applyButton()
	{
		this.applyButton = new JButton("Apply");
		this.applyButton.setMaximumSize(new Dimension(100,50));
		this.applyButton.addActionListener(this);
	}
	
	public void setDefaultSettings(File properties) throws FileNotFoundException
	{
		Scanner scan = new Scanner(properties);
		
		this.fontStyle = scan.next();//gets font style from file
		this.fontSize = scan.nextInt();//gets font size from file
		
		if(scan.nextBoolean())//if true then makes default bold
			this.isBold = true;
		
		if(scan.nextBoolean())//if true then makes default underlined
			this.isUnderlined = true;
		
		if(scan.nextBoolean())//if true then makes default Italic
			this.isItalic = true;
	}
	
	public void setSampleText(Font current)
	{
		
		currentFont = current;
		this.fontSampleText.setFont(current);
		
	}
	
	public void setFont()
	{
		System.out.println("the font has been set");
		switch(getFontID())
		{
		case 0:
			this.currentFont =  new Font(this.fontStyle, Font.PLAIN, this.fontSize);
			break;
		case 1:
			this.currentFont = new Font(this.fontStyle, Font.BOLD, this.fontSize);
			break;
		case 10:
			this.currentFont = new Font(this.fontStyle, Font.ITALIC, this.fontSize);
			break;
		case 11: 
			this.currentFont = new Font(this.fontStyle, Font.ITALIC & Font.BOLD, this.fontSize);
			break;
		default:
			System.out.println("Impossible entry");
			break;
		}
		
	}
	
	private int getFontID()
	{
		String bit = "";
		
		if(this.isBold)
			bit	= "1" + bit;
		else
			bit	= "0" + bit;
		
		if(this.isItalic)
			bit	= "1" + bit;
		else
			bit	= "0" + bit;
		
		if(this.isUnderlined)
			bit	= "1" + bit;
		else
			bit	= "0" + bit;
		
		return Integer.parseInt(bit);
		
	}

	protected String[] getFontFamilies()
	{
		
		if (fontNames == null)
		{
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			fontNames = env.getAvailableFontFamilyNames();
		}
		return fontNames;
	}
	

	public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == this.bold)
        {
        	this.isBold = !this.isBold;
        	System.out.println("bold checked");
        	setFont();
        	this.fontSampleText.setFont(this.currentFont);
 		   	this.fontSampleText.setText(this.testString);

        }else
        if(e.getSource() == this.italic)
        {

        	this.isItalic = !this.isItalic;
        	System.out.println("italic checked");
        	setFont();
        	this.fontSampleText.setFont(this.currentFont);
 		    this.fontSampleText.setText(this.testString);
        }else if(e.getSource() == this.applyButton)
       {
        	//appliedFont = this.currentFont;
        	System.out.println("apply check");
        	tempMainChat.setFontSetting(currentFont);	
       }else if(e.getSource() == this.fontSizeInput)
       {
    	   
    	   if(this.fontSizeInput.getText().matches("[0-9]+") && this.fontSizeInput.getText().length() > 0)
    	   {  
    		   this.fontSize = Integer.parseInt(this.fontSizeInput.getText());
    		   setFont();
    		   System.out.println(this.fontSize);
    		   this.fontSampleText.setFont(this.currentFont);
    		   this.fontSampleText.setText(this.testString);
    	   }
    	   //if letter is inputed the program will ignore the input
       }
       
       
       
    }
	
}
