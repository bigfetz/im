package com.stoof.IM.utils;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.stoof.IM.Stoof_IM;
import com.stoof.IM.GUI.MainChat;

/**
 * Utilities class for the application.
 * 
 * @author Stoof
 */
public class Utils implements ActionListener
{

    private static int return_value;
    static Font allFont;
    static MainChat tempChat;
    
    public Utils(MainChat temp)
    {
    	tempChat = temp;
    }

    public static void print( Object item )
    {
        System.out.println(item);
    }

    public static int quitMenuPopup( )
    {
        final String[] options = { "Yes", "Cancel" };
        final String message = "Are you sure you want to quit?";
        final String title = "Quit";
        return_value = 0;
        Runnable run = new Runnable()
        {
            @Override
            public void run( )
            {
                return_value = JOptionPane.showOptionDialog(null, message, title
                        , JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE
                        , null, options, options[0]);
            }
        };

        if (SwingUtilities.isEventDispatchThread())
        {
            run.run();
        }
        else
        {
            try
            {
                SwingUtilities.invokeAndWait(run);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
                e.printStackTrace();
            }
        }

        return return_value;
    }

    public static void saveFile( String text ) throws IOException
    {

        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date date = new Date();

        String name = ( String ) JOptionPane.showInputDialog(null, null
                , "Name File", JOptionPane.PLAIN_MESSAGE, null, null
                , ( String ) dateFormat.format(date));
        
        if (name != null)//catches the cancel so doesn't execute code if I cancel out of save
        {
            name = name + ".stoof";

            String dir = Stoof_IM.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            File file = new File(dir.substring(0, dir.length() - 4) + "savedFiles/" + name);
            if (!file.exists())
                file.createNewFile();

            Writer out = new BufferedWriter(new FileWriter(file, true));

            out.append(text);
            out.close();
        }
    }

    public static void openFile( ) throws FileNotFoundException
    {
        int check;
        Dimension d = new Dimension(750, 5000);
        File path = null;
        JFileChooser fileChooser = new JFileChooser();
        File f = new File("savedFiles");
        fileChooser.setCurrentDirectory(f);
        check = fileChooser.showOpenDialog(null);
        if (check == JFileChooser.APPROVE_OPTION)
        {
            path = fileChooser.getSelectedFile();
            Scanner scan = new Scanner(path);
            JDialog box = new JDialog();
            box.setSize(d);
            JTextArea text = new JTextArea();
            text.setSize(d);
            JScrollPane scroll_pane = new JScrollPane(text);
            text.setEditable(false);
            while (scan.hasNext())
                text.setText(text.getText() + "\n" + scan.nextLine());
            box.add(scroll_pane, BorderLayout.CENTER);
            box.setVisible(true);
        }

    }

    @Override
    public void actionPerformed( ActionEvent arg0 )
    {
        // TODO Auto-generated method stub

    }


}