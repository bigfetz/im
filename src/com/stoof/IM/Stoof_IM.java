package com.stoof.IM;

import java.awt.EventQueue;

import com.stoof.IM.GUI.GUI;

/**
 * The main program. Handles all main functions.
 * 
 * @author Matthew Fetzer
 * @version 1.1
 */
public class Stoof_IM
{

    /**
     * Launch the application.
     */
    public static void main( String[] args )
    {
        EventQueue.invokeLater(new Runnable()
        {
            public void run( )
            {
                try
                {
                    GUI frame = new GUI();
                    frame.setVisible(true);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

}